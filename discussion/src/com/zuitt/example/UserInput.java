package com.zuitt.example;


import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        // We instantiate the myObj form the Scanner class
        // Scanner is used for obtaining input from the terminal.
        // "System.in" allows us to takes input from the console.
        Scanner myObj = new Scanner(System.in);
//        System.out.println("Enter username:");
//        // to capture the input given by the user, we will user, the nextLine() method;
//        // Takes an input until the line changes or  new line was initiated with enter.
//        String userName = myObj.nextLine();
////        System.out.println("Username is: " + userName);
//        System.out.println(userName + userName);

//        System.out.println("Enter a number to add: ");
//        System.out.println("Enter first number: ");
        // for converting it to number
//        int num1 = Integer.parseInt(myObj.nextLine());
        int num1 = myObj.nextInt();
        System.out.println("Enter second number: ");
        int num2 = myObj.nextInt();
        System.out.println("The sum of two numbers is " + (num1 + num2));


    }
}
